﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    //Obstacles
    public Transform[] spawnLocations;
    public GameObject[] spawnPrefabs;
    public GameObject[] spawnClone;

    //Loot/Ammo
    
    //Values
    private int randomNo;
    private int cloneNo = 0;

    protected virtual void Awake()
    {
        SpawnEach();
    }

    public void SpawnEach()
    {   
        foreach(Transform i in spawnLocations)
        {
            randomNo = Random.Range(0, spawnPrefabs.Length);

            spawnClone[cloneNo] = Instantiate(spawnPrefabs[randomNo], spawnLocations[cloneNo].transform.position, Quaternion.identity) as GameObject;
            Debug.Log("CloneNo = " + cloneNo + " RandomNo = " + randomNo);

            spawnClone[cloneNo].transform.SetParent(this.transform);
            DontDestroyOnLoad(spawnClone[cloneNo]);

            cloneNo++;
        }
    }
    

    /*
    void SpawnUnit(GameObject prefabUnit, string name, float health, float moveSpeed)
    {
        GameObject newEnemy = (GameObject)Instantiate(prefabUnit, Vector3.one * Random.Range(-5, 5), Quaternion.identity);
        enemyList.Add(newEnemy);

        newEnemy.GetComponent<Unit>().Init(name, health, moveSpeed);
        
    }
    */

    //Loot and Obstacles Spawner

    //public int numberToSpawn;
    //public List<GameObject> spawnPool;
    //public GameObject quad;

    //public void spawnObjects()
    //{
    //    int randomItem = 0;
    //    GameObject toSpawn;
    //    BoxCollider2D c = quad.GetComponent<BoxCollider2D>();

    //    float screenX, screenY;
    //    Vector2 pos;

    //    for(int i = 0; i < numberToSpawn; i++)
    //    {
    //        randomItem = Random.Range(0, spawnPool.Count);
    //        toSpawn = spawnPool[randomItem];

    //        screenX = Random.Range(c.bounds.min.x, c.bounds.max.x);
    //    }
    //}
}

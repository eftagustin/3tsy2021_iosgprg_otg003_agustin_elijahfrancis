﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAim : MonoBehaviour
{

    public static PlayerAim instance;

    //private Rigidbody myBody;
    //public float rotateVertical;
    //public float rotateHorizontal;

    //private FixedJoystick joystick;

    //void Awake()
    //{
    //    myBody = GetComponent<Rigidbody>();
    //    joystick = GameObject.FindWithTag("AimJoystick").GetComponent<FixedJoystick>();
    //}

    //// Update is called once per frame
    //void FixedUpdate()
    //{
    //    rotateVertical = joystick.Vertical * 1f;
    //    rotateHorizontal = joystick.Horizontal * -1f;

    //    myBody.transform.Rotate(rotateHorizontal, rotateVertical, 0f);
    //}

    private FixedJoystick joystick;

    private void Awake()
    {

     
            if (instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = this;
            }

            //DontDestroyOnLoad(this);
        


    }
    public void SetJoystick()
    {
        joystick = GameObject.FindWithTag("AimJoystick").GetComponent<FixedJoystick>();
    }


    private void Update()
    {
        Vector3 moveVector = (Vector3.up * joystick.Horizontal + Vector3.left * joystick.Vertical);

        if(joystick.Horizontal != 0 || joystick.Vertical != 0)
        {
            transform.rotation = Quaternion.LookRotation(Vector3.forward, moveVector);
        }
    }

}

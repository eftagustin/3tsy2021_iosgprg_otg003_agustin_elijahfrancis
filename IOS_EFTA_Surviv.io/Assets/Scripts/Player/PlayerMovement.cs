﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public static PlayerMovement instance;

    private Rigidbody2D myBody;
    public float moveForce = 7.5f;

    public FixedJoystick joystick;

    //private PlayerAnimation anim;

    void Awake()
    {
        myBody = GetComponent<Rigidbody2D>();

            if (instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = this;
            }

            //DontDestroyOnLoad(this);
    }

    public void SetJoystick()
    {
        joystick = GameObject.FindWithTag("Joystick").GetComponent<FixedJoystick>();
    }

    // Update is called once per frame
    void Update()
    {
        myBody.velocity = new Vector2(joystick.Horizontal * moveForce,
                                        joystick.Vertical * moveForce);
    }

}

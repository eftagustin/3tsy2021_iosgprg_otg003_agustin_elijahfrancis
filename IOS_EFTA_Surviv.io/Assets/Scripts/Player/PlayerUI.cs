﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PlayerUI : MonoBehaviour
{
    public Player player;

    public GameObject clipNumPanel;
    public GameObject bagNumPanel;

    public TextMeshProUGUI clipCount;
    public TextMeshProUGUI bagCount;

    public Button shotgunBtn;
    public Button pistolBtn;
    public Button awBtn;
    public EventTrigger trigger;

    public Button fireButton;
    public Button reloadButton;
    public GameObject awPanel;

    void Start()
    {
        shotgunBtn.gameObject.SetActive(false);
        pistolBtn.gameObject.SetActive(false);
        awBtn.gameObject.SetActive(false);

        clipNumPanel.gameObject.SetActive(false);
        bagNumPanel.gameObject.SetActive(false);

        fireButton.gameObject.SetActive(false);
        reloadButton.gameObject.SetActive(false);
        awPanel.gameObject.SetActive(false);

        //player = GameObject.FindObjectOfType<Player>();
        //player = PGameManager.instance.player;
        //player = GameManager.instance.player;

        fireButton.onClick.AddListener(() => {

            player?.PrimaryShoot();
            Debug.Log("Primary shoot button notice aaa");

            });

        //yield return new WaitUntil(() => player != null);

        reloadButton.onClick.AddListener(() => {

            player?.Reload();
            Debug.Log("Reload button notice aaa");

        });

        trigger = fireButton.gameObject.GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerDown;
        entry.callback.AddListener((data) => {
            AutoWeap autoWeap = player.autoWeap.gun as AutoWeap;

            autoWeap?.OnPointerDown();
            //player?.PrimaryShoot();

            Debug.Log("AW OnPointerDown button notice aaa");
        });
        trigger.triggers.Add(entry);

        EventTrigger.Entry exit = new EventTrigger.Entry();
        exit.eventID = EventTriggerType.PointerUp;

        exit.callback.AddListener((data) => {
            AutoWeap autoWeap = player.autoWeap.gun as AutoWeap;

            autoWeap?.OnPointerUp();
            });

        trigger.triggers.Add(exit);

    }

    void Update()
    {
        SetPlayer();

        UpdateAmmoUI();

        //change to event switching this
        CheckEquipmentAcquired();
    }

    public void CheckEquipmentAcquired()
    {
        if(player.activeWeapon != player.noWeap)
        {
            fireButton.gameObject.SetActive(true);
            reloadButton.gameObject.SetActive(true);

            clipNumPanel.gameObject.SetActive(true);
            bagNumPanel.gameObject.SetActive(true);
        }

        if (player.autoWeap.isAcquired)
        {
            awBtn.gameObject.SetActive(true);
        }

        if (player.shotgun.isAcquired)
        {
            shotgunBtn.gameObject.SetActive(true);
        }

        if (player.pistol.isAcquired)
        {
            pistolBtn.gameObject.SetActive(true);
        }
    }

    public void UpdateAmmoUI()
    {
        if (player.activeWeapon.gun is AutoWeap)
        {
            clipCount.text = player.ammoAW.currentClipCount.ToString();
            bagCount.text = player.ammoAW.currentHeldCount.ToString();
        }
        else if (player.activeWeapon.gun is Shotgun)
        {
            clipCount.text = player.ammoShotgun.currentClipCount.ToString();
            bagCount.text = player.ammoShotgun.currentHeldCount.ToString();
        }
        else if (player.activeWeapon.gun is Pistol)
        {
            clipCount.text = player.ammoPistol.currentClipCount.ToString();
            bagCount.text = player.ammoPistol.currentHeldCount.ToString();
        }
        else if (player.activeWeapon.gun is Gun)
        {
            clipCount.text = "N/A";
            bagCount.text = "N/A";
        }
    }

    public void SwitchToShotgun()
    {
        if (player.shotgun.isAcquired == true)
        {
            player.activeWeapon = player.shotgun;
            Debug.Log("Current shotgun");
            awPanel.gameObject.SetActive(false);
        }
    }

    public void SwitchToPistol()
    {
        if (player.pistol.isAcquired == true)
        {
            player.activeWeapon = player.pistol;
            Debug.Log("Current pistol");
            awPanel.gameObject.SetActive(false);
        }
    }

    public void SwitchToAW()
    {
        if (player.autoWeap.isAcquired == true)
        {
            player.activeWeapon = player.autoWeap;
            Debug.Log("Current AW");
            awPanel.gameObject.SetActive(true);
        }
    }

    public void AwContShoot()
    {
        if (Input.GetButton("Fire_Button"))
        {
            Debug.Log("Cont fire AW");
            player.activeWeapon.gun.Fire(player.ammoAW, player);
        }
    }

    void SetPlayer()
    {
        if (player == null && GameManager.instance != null)
        {
            player = GameManager.instance.player;
        }
    }
}

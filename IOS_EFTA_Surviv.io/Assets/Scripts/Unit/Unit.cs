﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Health))]
//[RequireComponent(typeof(Equipment))]

public class Unit : MonoBehaviour
{
    public string unitName;

    public Health health;

    public float moveSpeed;

    public Equipment shotgun;
    public Equipment pistol;
    public Equipment autoWeap;

    public Equipment noWeap;

    public Equipment activeWeapon;

    public AmmoPistol ammoPistol;
    public AmmoAW ammoAW;
    public AmmoShotgun ammoShotgun;

    public Bullet bullet;

    public Action OnDeath;


    //-------------------------------------------//

    private void Awake()
    {
        
    }

    private void Start()
    {
        //shotgun.gun.gameObject.SetActive(false);
        //pistol.gun.gameObject.SetActive(false);
        //autoWeap.gun.gameObject.SetActive(false);
    }

    public void Init(string nName, float nHealth, float nMoveSpeed)
    {
        //should I also init the Ammo types? Equips?

        this.name = nName;
        this.moveSpeed = nMoveSpeed;

        health = this.GetComponent<Health>();
        health.maxValue = nHealth;
        //equipment = this.GetComponent<Equipment>();

        Debug.Log("[Init] Unit Created " + nName);

        OnDeath += Death;
    }

    public void Death()
    {
       if(health.currentValue <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    //Split out
    public void PrimaryShoot()
    {
        //Ammo check here

        if (activeWeapon.gun != null)
        {
            //called by primary shoot button

            //expend proper ammo

            if (activeWeapon.gun is Shotgun)
            {
                //Debug.Log(name + " has shot SHG.");
                activeWeapon.gun.Fire(ammoShotgun, this);
            }
            else if (activeWeapon.gun is AutoWeap)
            {
                //needs to continuously fire is button still down
                //Debug.Log(name + " has shot AW.");
                activeWeapon.gun.Fire(ammoAW, this);
            }
            else if (activeWeapon.gun is Pistol)
            {
                //Debug.Log(name + " has shot Pistol.");
                activeWeapon.gun.Fire(ammoPistol,this);
            }
        }
        else if(activeWeapon.gun == null)
        {
            Debug.Log("Cannot fire. No weapon!");
        }
    }

    public virtual void Reload()
    {
        //button
        //reloads primary weap
        if(activeWeapon.gun is Shotgun && ammoShotgun.currentHeldCount > 0)
        {
            //semi auto
            activeWeapon.gun.Reload(ammoShotgun);
        }
        else if(activeWeapon.gun is AutoWeap && ammoAW.currentHeldCount > 0)
        {
            //auto
            //reloads on its own
            activeWeapon.gun.Reload(ammoAW);
        }
        else if(activeWeapon.gun is Pistol && ammoPistol.currentHeldCount > 0)
        {
            //semi auto
            activeWeapon.gun.Reload(ammoPistol);
        }
        else if(activeWeapon.gun == null)
        {
            Debug.Log("Cannot reload. No gun!");
        }
        else
        {
            Debug.Log("Cannot reload. No ammo!");
        }

    }

    //Redundant?
    public void TakeDamage(int damageValue)
    {
        health.currentValue -= damageValue;

        if(health.currentValue <= 0)
        {
            OnDeath.Invoke();
        }
    }

    //SEPARATE INTO COMPO
    public virtual void OnTriggerEnter2D(Collider2D collision)
    {
        //PICKUP
        if(collision.tag == "Ammo1")
        {
            ammoPistol.currentHeldCount += 5;
            Destroy(collision.gameObject);
        }

        else if (collision.tag == "Ammo2")
        {
            ammoShotgun.currentHeldCount += 5;
            Destroy(collision.gameObject);
        }

        else if (collision.tag == "Ammo3")
        {
            ammoAW.currentHeldCount += 5;
            Destroy(collision.gameObject);
        }

        if(collision.tag == "Pistol")
        {
            //    this.pistol.gun.gameObject.SetActive(true);
            pistol.isAcquired = true;
            Destroy(collision.gameObject);
        }
        else if (collision.tag == "Shotgun")
        {
            //    this.shotgun.gun.gameObject.SetActive(true);
            shotgun.isAcquired = true;
            Destroy(collision.gameObject);
        }
        else if (collision.tag == "AW")
        {
            //    this.autoWeap.gun.gameObject.SetActive(true);
            autoWeap.isAcquired = true;
            Destroy(collision.gameObject);
        }

        /*//DAMAGE
        if (collision.gameObject.tag == "P_Bullet")
        {
            bullet = collision.gameObject.GetComponent<Bullet>();
            if (bullet.owner != this.gameObject)
            {
                TakeDamage(10);
                
            }

            Debug.Log(name + " has been shot by Pistol. Current HP is " + health.currentValue);
        }
        else if (collision.gameObject.tag == "A_Bullet")
        {
            bullet = collision.gameObject.GetComponent<Bullet>();
            if (bullet.owner != this.gameObject)
            {
                TakeDamage(15);
            }
            Debug.Log(name + " has been shot by AW. Current HP is " + health.currentValue);
        }
        else if (collision.gameObject.tag == "S_Bullet")
        {
            bullet = collision.gameObject.GetComponent<Bullet>();
            if (bullet.owner != this.gameObject)
            {
                TakeDamage(20);
            }
            Debug.Log(name + " has been shot by SHG. Current HP is " + health.currentValue);
        }
        */
    }

}

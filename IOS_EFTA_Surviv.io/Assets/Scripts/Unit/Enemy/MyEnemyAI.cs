﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyEnemyAI : MonoBehaviour
{
    public enum State
    {
        Roaming,
        ChaseTarget,
        Looting
    }

    private Vector3 startingPosition;
    public State state;

    public float speed;

    //roam
    public Transform[] moveSpots;
    private int randomSpot;
    private float waitTime;
    public float startWaitTime;
    private float roamTime;
    public float startRoamTime;

    //chase
    public float stoppingDist;
    public float retreatDist;
    public float detectRadius;


    //fire
    public CircleCollider2D detectionCircle;
    private float timeBetShots;
    public float startTimeBetShots;
    public Unit targetUnit;
    public GameObject targetItem;
    private Vector2 movement;

    public Player player;
    public Enemy enemy;
    private Rigidbody2D rb;

    public void Awake()
    {
        state = State.Roaming;        
    }

    void Start()
    {
        player = GameManager.instance.player;
        moveSpots = GameManager.instance.moveSpots;
        enemy = this.gameObject.GetComponent<Enemy>();
        rb = enemy.enemyRigidbody2D;

        startingPosition = transform.position;

        waitTime = startWaitTime;
        timeBetShots = startTimeBetShots;
        randomSpot = Random.Range(0, moveSpots.Length);
        roamTime = startRoamTime;
    }
    
    void Update()
    {
        StateMachine();   
    }

    void SwitchState()
    {
        if (targetUnit != null)
        {
            if (Vector2.Distance(transform.position, targetUnit.transform.position) < detectRadius)
            {
                //Should be Firing right away
                state = State.ChaseTarget;
            }
            else if (Vector2.Distance(transform.position, player.transform.position) > detectRadius)
            {
                state = State.Roaming;
            }
            else if(targetItem != null)
            {
                state = State.Looting;
            }
        }
    }

    void StateMachine()
    {
        SwitchState();

        switch (state)
        {
            default:
            case State.Roaming:

                transform.position = Vector2.MoveTowards(transform.position, moveSpots[randomSpot].position, speed * Time.deltaTime);

                Vector3 direction = moveSpots[randomSpot].position - transform.position;
                float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
                rb.rotation = angle;
                direction.Normalize();

                ///choosing a spot
                ///1 - roamTime, time from point A to point B
                ///if enemy gets stuck, will eventually choose a new spot
                if (Vector2.Distance(transform.position, moveSpots[randomSpot].position) > 0.2f && roamTime >0)
                {
                    roamTime -= Time.deltaTime;
                }
                else if(roamTime <= 0)
                {
                    randomSpot = Random.Range(0, moveSpots.Length);
                    roamTime = startRoamTime;
                }
                ///here, enemy has reached spot, and has a waitTime before moving again
                ///this is the IDLE part of the enemy AI
                else if(Vector2.Distance(transform.position, moveSpots[randomSpot].position) < 0.2f)
                {
                    if (waitTime <= 0f)
                    {
                        randomSpot = Random.Range(0, moveSpots.Length);
                        waitTime = startWaitTime;
                    }
                    else
                    {
                        waitTime -= Time.deltaTime;
                    }
                }

                //change to idle


                break;

            case State.ChaseTarget:
                //Before chasing, rotate first
                //Fire right away!

                //if (Vector2.Distance(transform.position, player.transform.position) > stoppingDist)
                //{
                //    transform.position = Vector2.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
                //}
                //else if (Vector2.Distance(transform.position, player.transform.position) < stoppingDist && Vector2.Distance(transform.position, player.transform.position) > retreatDist)
                //{
                //    transform.position = transform.position;
                //}
                //else if (Vector2.Distance(transform.position, player.transform.position) < retreatDist)
                //{
                //    transform.position = Vector2.MoveTowards(transform.position, player.transform.position, -speed * Time.deltaTime);
                //}

                //1 - rotate to target
                if(targetUnit == null)
                {
                    return;
                }
                Vector3 chaseDirection = targetUnit.transform.position - transform.position;


                float chaseAngle = Mathf.Atan2(chaseDirection.y, chaseDirection.x) * Mathf.Rad2Deg;
                rb.rotation = chaseAngle;
                chaseDirection.Normalize();
                movement = chaseDirection;

                //2 - shoot
               
                    if (enemy.activeWeapon.gun is AutoWeap && timeBetShots <= 0)
                    {
                        enemy.activeWeapon.gun.Fire(enemy.ammoAW, enemy);
                        timeBetShots = startTimeBetShots/2.0f;

                        if (enemy.ammoAW.currentClipCount <= 0 && enemy.ammoAW.currentHeldCount > 0)
                        {
                            Debug.Log("enemy reloaded");
                            enemy.activeWeapon.gun.Reload(enemy.ammoAW);
                        }
                         else if (enemy.ammoAW.currentClipCount <= 0 && enemy.ammoAW.currentHeldCount <= 0)
                         {
                             SwitchGun();
                         }

                    }
                    else if (enemy.activeWeapon.gun is Shotgun && timeBetShots <= 0)
                    {
                        enemy.activeWeapon.gun.Fire(enemy.ammoShotgun, enemy);
                        timeBetShots = startTimeBetShots;

                    if (enemy.ammoShotgun.currentClipCount <= 0 && enemy.ammoShotgun.currentHeldCount > 0)
                        {
                            Debug.Log("enemy reloaded");
                            enemy.activeWeapon.gun.Reload(enemy.ammoShotgun);
                        }
                        else if (enemy.ammoShotgun.currentClipCount <= 0 && enemy.ammoShotgun.currentHeldCount <= 0)
                            {
                            SwitchGun();
                        }

                    }
                    else if (enemy.activeWeapon.gun is Pistol && timeBetShots <= 0)
                    {
                        enemy.activeWeapon.gun.Fire(enemy.ammoPistol, enemy);
                        timeBetShots = startTimeBetShots;

                        if (enemy.ammoPistol.currentClipCount <= 0 && enemy.ammoPistol.currentHeldCount > 0)
                         {
                            Debug.Log("enemy reloaded");
                            enemy.activeWeapon.gun.Reload(enemy.ammoPistol);
                         }
                         else if(enemy.ammoPistol.currentClipCount <= 0 && enemy.ammoPistol.currentHeldCount <= 0)
                         {
                            SwitchGun();
                         }
                    }

                timeBetShots -= Time.deltaTime;
               
                break;

            case State.Looting:
                //when an ammo object is in my radius
                //go there
                //then roam again

                //BULLETS
                if (targetItem == null)
                {
                    state = State.Roaming;
                    return;
                }
           

                //moving towards the loot
                if (Vector2.Distance(transform.position, targetItem.transform.position) > stoppingDist)
                {
                    transform.position = Vector2.MoveTowards(transform.position, targetItem.transform.position, speed * Time.deltaTime);
                }
                else if (Vector2.Distance(transform.position, targetItem.transform.position) < stoppingDist)
                {
                    Debug.Log("Got target item");
                    Destroy(targetItem);
                    state = State.Roaming;
                }

                //GUNS

                break;
        }
    }

    void SwitchGun()
    {
        Debug.Log("went to switch gun");

        if (enemy.activeWeapon.gun is Shotgun)
        {
            if (enemy.pistol.isAcquired
                && (enemy.ammoPistol.currentHeldCount > 0
                || enemy.ammoPistol.currentClipCount > 0))
            {
                Debug.Log(this.gameObject.name + " switched SHG to PIS.");
                enemy.activeWeapon = enemy.pistol;
            }
            if (enemy.autoWeap.isAcquired
                && (enemy.ammoAW.currentHeldCount > 0
                || enemy.ammoAW.currentClipCount > 0))
            {
                Debug.Log(this.gameObject.name + " switched SHG to AW.");
                enemy.activeWeapon = enemy.autoWeap;
            }
        }
        else if (enemy.activeWeapon.gun is Pistol)
        {
            if (enemy.shotgun.isAcquired
               && (enemy.ammoShotgun.currentHeldCount > 0
                || enemy.ammoShotgun.currentClipCount > 0))
            {
                Debug.Log(this.gameObject.name + " switched PIS to SHG.");
                enemy.activeWeapon = enemy.shotgun;
            }
            if (enemy.autoWeap.isAcquired
                && (enemy.ammoAW.currentHeldCount > 0
                || enemy.ammoAW.currentClipCount > 0))
            {
                Debug.Log(this.gameObject.name + " switched PIS to AW.");
                enemy.activeWeapon = enemy.autoWeap;
            }
        }
        else if (enemy.activeWeapon.gun is AutoWeap)
        {
  
            if (enemy.pistol.isAcquired
                && (enemy.ammoPistol.currentHeldCount > 0
                || enemy.ammoPistol.currentClipCount > 0))
            {
                //enemy.GetComponent<AutoWeap>().StopProcesses();
                Debug.Log(this.gameObject.name + " switched AW to PIS.");
                enemy.activeWeapon = enemy.pistol;
            }
            if (enemy.shotgun.isAcquired
                && (enemy.ammoShotgun.currentHeldCount > 0
                || enemy.ammoShotgun.currentClipCount > 0))
            {
                //enemy.GetComponent<AutoWeap>().StopProcesses();

                Debug.Log(this.gameObject.name + " switched AW to SHG.");
                enemy.activeWeapon = enemy.shotgun;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Enemy" && collision.gameObject != this.gameObject || collision.tag == "Player")
        {
            Debug.Log("Firing target is " + collision.gameObject.name);
            targetUnit = collision.gameObject.GetComponent<Unit>();
            state = State.ChaseTarget;
        }
        else if(collision.tag == "Ammo1" || collision.tag == "Ammo2" || collision.tag == "Ammo3"
                || collision.tag == "Pistol" || collision.tag == "Shotgun" || collision.tag == "AW")
        {
            //ADD GUNS HERE
            Debug.Log("State Looting triggered");
            targetItem = collision.gameObject;
            state = State.Looting;
        }
    }
}

///Sources
///CodeMonkey - unitycodemonkey.com/video.php?v=db0KWYaWfeM
///Blackthornprod - www.youtube.com/watch?v=_Z1t7MNk0c4
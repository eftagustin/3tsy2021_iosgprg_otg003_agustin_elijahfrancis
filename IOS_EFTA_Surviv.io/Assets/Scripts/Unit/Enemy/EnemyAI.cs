﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    private enum State
    {
        Roaming,
        ChaseTarget,
        ShootingTarget,
        GoingBackToStart,
    }

    private EnemyPathfinding pathfindingMovement;
    private Vector3 startingPosition;
    private Vector3 roamPosition;
    private float nextShootTime;
    private State state;

    private void Awake()
    {
        pathfindingMovement = GetComponent<EnemyPathfinding>();
        state = State.Roaming;
    }

    void Start()
    {
        startingPosition = transform.position;
        roamPosition = GetRoamingPosition();
    }

    void Update()
    {
        StateMachine();
    }

    void StateMachine()
    {
        switch (state)
        {
            default:
            case State.Roaming:
                pathfindingMovement.MoveToTimer(roamPosition);

                float reachedPositionDistance = 10f;

                if (Vector3.Distance(transform.position, roamPosition) < reachedPositionDistance)
                {
                    // Reached Roam Position
                    roamPosition = GetRoamingPosition();
                }

                FindTarget();
                break;

            case State.ChaseTarget:
                pathfindingMovement.MoveToTimer(GameManager.instance.player.transform.position);

                float attackRange = 30f;
                if (Vector3.Distance(transform.position, GameManager.instance.player.transform.position) < attackRange)
                {
                    // Target within attack range
                    if (Time.time > nextShootTime)
                    {
                        pathfindingMovement.StopMoving();
                        state = State.ShootingTarget;

                        
                        //aimShootAnims.ShootTarget(Player.Instance.GetPosition(), () => {
                            state = State.ChaseTarget;
                        //});
                        float fireRate = .15f;
                        nextShootTime = Time.time + fireRate;
                    }
                }

                float stopChaseDistance = 80f;
                if (Vector3.Distance(transform.position, GameManager.instance.player.transform.position) > stopChaseDistance)
                {
                    // Too far, stop chasing
                    state = State.GoingBackToStart;
                }
                break;

            case State.ShootingTarget:
                break;

            case State.GoingBackToStart:
                pathfindingMovement.MoveToTimer(startingPosition);

                reachedPositionDistance = 10f;
                if (Vector3.Distance(transform.position, startingPosition) < reachedPositionDistance)
                {
                    // Reached Start Position
                    state = State.Roaming;
                }
                break;
        }
    }

    private Vector3 GetRoamingPosition()
    {
        return startingPosition + GetRandomDir() * Random.Range(10f, 70f);
    }

    public Vector3 GetRandomDir()
    {
        return new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized;
    }

    private void FindTarget()
    {
        float targetRange = 50f;
        if (Vector3.Distance(transform.position, GameManager.instance.player.transform.position) < targetRange)
        {
            // Player within target range
            state = State.ChaseTarget;
        }
    }
}

///Source:
///Simple Enemy AI in Unity - Code Monkey YouTube
///www.youtube.com/watch?v=db0KWYaWfeM
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Unit
{
    private int randomStartWeaponNo;
    public Rigidbody2D enemyRigidbody2D;

    void Start()
    {
        SetActiveWeapon();
        Init("Enemy", 100, 5);
    }


    void Update()
    {

    }

    public void SetActiveWeapon()
    {
        randomStartWeaponNo = Random.Range(0, 2);

        if (randomStartWeaponNo == 0 && shotgun != null)
        {
            activeWeapon = shotgun;
            shotgun.isAcquired = true;
        }
        else if (randomStartWeaponNo == 1 && autoWeap != null)
        {
            activeWeapon = autoWeap;
            autoWeap.isAcquired = true;
        }
        else if (randomStartWeaponNo == 2 && pistol != null)
        {
            activeWeapon = pistol;
            pistol.isAcquired = true;
        }
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        //PICKUP
        if (collision.tag == "Ammo1")
        {
            ammoPistol.currentHeldCount += 5;
            Destroy(collision.gameObject);
        }

        else if (collision.tag == "Ammo2")
        {
            ammoShotgun.currentHeldCount += 5;
            Destroy(collision.gameObject);
        }

        else if (collision.tag == "Ammo3")
        {
            ammoAW.currentHeldCount += 5;
            Destroy(collision.gameObject);
        }

        if (collision.tag == "Pistol")
        {
            //    this.pistol.gun.gameObject.SetActive(true);
            pistol.isAcquired = true;
            Destroy(collision.gameObject);
        }
        else if (collision.tag == "Shotgun")
        {
            //    this.shotgun.gun.gameObject.SetActive(true);
            shotgun.isAcquired = true;
            Destroy(collision.gameObject);
        }
        else if (collision.tag == "AW")
        {
            //    this.autoWeap.gun.gameObject.SetActive(true);
            autoWeap.isAcquired = true;
            Destroy(collision.gameObject);
        }

        //DAMAGE
        if (collision.gameObject.tag == "P_Bullet")
        {
            bullet = collision.gameObject.GetComponent<Bullet>();
            if (bullet.owner != this)
            {
                TakeDamage(10);
                Debug.Log(name + " has been shot by Pistol. Current HP is " + health.currentValue);
            }

            //Debug.Log(name + " has been shot by Pistol. Current HP is " + health.currentValue);
        }
        else if (collision.gameObject.tag == "A_Bullet")
        {
            bullet = collision.gameObject.GetComponent<Bullet>();
            if (bullet.owner != this)
            {
                TakeDamage(15);
                Debug.Log(name + " has been shot by AW. Current HP is " + health.currentValue);
            }
            //Debug.Log(name + " has been shot by AW. Current HP is " + health.currentValue);
        }
        else if (collision.gameObject.tag == "S_Bullet")
        {
            bullet = collision.gameObject.GetComponent<Bullet>();
            if (bullet.owner != this)
            {
                TakeDamage(20);
                Debug.Log(name + " has been shot by SHG. Current HP is " + health.currentValue);
            }
            //Debug.Log(name + " has been shot by SHG. Current HP is " + health.currentValue);
        }

    }
}
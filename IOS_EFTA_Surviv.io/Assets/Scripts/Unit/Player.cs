﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.EventSystems;

public class Player : Unit
{
    private Equipment weaponHolder;

    void Start()
    {
        //Initialization of Player Char
        Init("PlayerUnit", 100, 5);

        //Passing PlayerUnit reference to GM
        GameManager.instance.InitPlayer(this);
        Debug.Log("Player told GM to Init");

        //SetActiveWeapon
        activeWeapon = noWeap;

        //GameManager.instance.player = this;
        PGameManager.instance.player = this;

        OnDeath += PGameManager.instance.GameOver;
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        //PICKUP
        if (collision.tag == "Ammo1")
        {
            ammoPistol.currentHeldCount += 5;
            Destroy(collision.gameObject);
        }

        else if (collision.tag == "Ammo2")
        {
            ammoShotgun.currentHeldCount += 5;
            Destroy(collision.gameObject);
        }

        else if (collision.tag == "Ammo3")
        {
            ammoAW.currentHeldCount += 5;
            Destroy(collision.gameObject);
        }

        if (collision.tag == "Pistol")
        {
            pistol.isAcquired = true;
            Destroy(collision.gameObject);
        }
        else if (collision.tag == "Shotgun")
        {
            shotgun.isAcquired = true;
            Destroy(collision.gameObject);
        }
        else if (collision.tag == "AW")
        {
            autoWeap.isAcquired = true;
            Destroy(collision.gameObject);
        }

        //DAMAGE
        if (collision.gameObject.tag == "P_Bullet")
        {
            bullet = collision.gameObject.GetComponent<Bullet>();
            if (bullet.owner != this)
            {
                //take bullet int damage from bullet itself
                TakeDamage(10);
                Debug.Log(name + " has been shot by Pistol. Current HP is " + health.currentValue);
            }
        }
        else if (collision.gameObject.tag == "A_Bullet")
        {
            bullet = collision.gameObject.GetComponent<Bullet>();
            if (bullet.owner != this)
            {
                TakeDamage(15);
                Debug.Log(name + " has been shot by AW. Current HP is " + health.currentValue);
            }
        }
        else if (collision.gameObject.tag == "S_Bullet")
        {
            bullet = collision.gameObject.GetComponent<Bullet>();
            if (bullet.owner != this)
            {
                TakeDamage(20);
                Debug.Log(name + " has been shot by SHG. Current HP is " + health.currentValue);
            }
        }

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunDisplay : MonoBehaviour
{
    public Unit unit;

    public GameObject pistolGun;
    public GameObject awGun;
    public GameObject shotgunGun;

    void Start()
    {
        pistolGun.gameObject.SetActive(false);
        awGun.gameObject.SetActive(false);
        shotgunGun.gameObject.SetActive(false);
    }

 
    void Update()
    {
        //change to event later
        CheckDisplay();
    }

    public void CheckDisplay()
    {
        if (unit.activeWeapon.gun == unit.noWeap)
        {
            pistolGun.gameObject.SetActive(false);
            awGun.gameObject.SetActive(false);
            shotgunGun.gameObject.SetActive(false);
        }
        else if (unit.activeWeapon.gun is AutoWeap)
        {
            pistolGun.gameObject.SetActive(false);
            awGun.gameObject.SetActive(true);
            shotgunGun.gameObject.SetActive(false);
        }
        else if (unit.activeWeapon.gun is Shotgun)
        {
            pistolGun.gameObject.SetActive(false);
            awGun.gameObject.SetActive(false);
            shotgunGun.gameObject.SetActive(true);
        }
        else if (unit.activeWeapon.gun is Pistol)
        {
            pistolGun.gameObject.SetActive(true);
            awGun.gameObject.SetActive(false);
            shotgunGun.gameObject.SetActive(false);
        }
    } 
}

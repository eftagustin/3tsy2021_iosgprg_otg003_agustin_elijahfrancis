﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equipment : MonoBehaviour
{
    //What the Unit carries
    //not Gun properties

    ///Unnecessary, ammo details are in AmmoGun derived classes
    //public int numberOfClips;
    //public int clipBulletAmount;

    public Gun gun;
    public bool isAcquired;

    //public AmmoPistol ammoPistol;
    //public AmmoAW ammoAW;
    //public AmmoShotgun ammoShotgun;

    void Start()
    {
        isAcquired = false;
    }


    void Update()
    {
        
    }
}

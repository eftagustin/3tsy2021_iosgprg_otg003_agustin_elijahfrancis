﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutoWeap : Gun
{
    //PLACEHOLDER

    //Just the behavior of firing
    //No access to # of bullets/clips

    public Unit unit;

    public Bullet a_bullet;
    public GameObject a_bulletPrefab;
    public Transform firePoint;

    public Bullet new_aBullet;

    public bool isFiring;
    public bool stopFiring;
    public float fireRate;

    public Button reloadBtn;

    public void Start()
    {
        fireRate = 0.4f;  
    }

    private void Update()
    {
        if (unit.ammoAW.currentClipCount > 0 && isFiring)
        {
            Debug.Log("am Firing " + unit.ammoAW.currentClipCount);
            Fire(unit.ammoShotgun, unit);
            SetContFireFalse();
        }
        else if (unit is Player &&
            (unit.ammoAW.currentClipCount <= 0) && 
            (unit.ammoAW.currentHeldCount > 0))
        {
            Reload(unit.ammoAW);
        }
    }

    public override void Fire(Ammo ammo, Unit owner)
    {
        if (ammo.currentClipCount > 0)
        {
            //Set Pistol bullet damage value
            damagePerBullet = 20;

            //Minus the # of bullet expended per fire
            ammo.currentClipCount -= 1;

            //Actual bullet instantiate
            //make 8 
            Vector3 bulletSpread = new Vector3(firePoint.position.x,
                firePoint.position.y + (Random.Range(0, 5) * 0.05f), firePoint.position.z);

            Quaternion bulletRotation = new Quaternion();
            float x = Random.Range(-3, 3);
            bulletRotation.eulerAngles = new Vector3(0, 0, x);

            firePoint.rotation.eulerAngles.Set(bulletRotation.eulerAngles.x,
                                                bulletRotation.eulerAngles.y,
                                                firePoint.rotation.eulerAngles.z + bulletRotation.eulerAngles.z);

            new_aBullet = Instantiate(a_bullet, bulletSpread, firePoint.rotation);
            new_aBullet.transform.SetParent(this.transform);
            DontDestroyOnLoad(new_aBullet);

            Debug.Log("Fired AW.");

            a_bullet.owner = owner;
        }
        //Automatic function
        else if(ammo.currentClipCount == 0 && ammo.currentHeldCount > 0)
        {
            Reload(ammo);
        }
    }

    IEnumerator FireWait()
    {
        yield return new WaitForSeconds(1.5f);
    }

    public override void Reload(Ammo ammo)
    {
        StartCoroutine(ReloadCoroutine(ammo));
    }

    public void StopProcesses()
    {
        StopAllCoroutines();
        OnPointerUp();
    }

    IEnumerator ReloadCoroutine(Ammo ammo)
    {
        if (unit.activeWeapon.gun is AutoWeap)
        {
            //RELOAD TIME HERE

            Debug.Log("AW reloading...");

            if (unit is Player)
            {
                reloadBtn.interactable = false;
            }

            yield return new WaitForSeconds(3.5f);

            int reloadAmount = ammo.maxClipCount - ammo.currentClipCount;

            if (ammo.currentHeldCount < reloadAmount)
            {
                reloadAmount = ammo.currentHeldCount;
            }

            ammo.currentClipCount += reloadAmount;
            ammo.currentHeldCount -= reloadAmount;

            Debug.Log("AW reloaded.");

            reloadBtn.interactable = true;
        }
    }

    public void SetContFireTrue()
    {
        isFiring = true;
    }

    public void SetContFireFalse()
    {
        isFiring = false;

        if(stopFiring == false)
        {
            unit.ammoAW.currentClipCount -= 1;
            Invoke("SetContFireTrue", fireRate);
        }
    }

    public void OnPointerDown()
    {
        if (unit.activeWeapon.gun is AutoWeap)
        {
            stopFiring = false;
            //unit.ammoAW.currentClipCount -= 1;
            Invoke("SetContFireTrue", fireRate);

            Fire(unit.ammoShotgun, unit);
        }
    }

    public void OnPointerUp()
    {
        stopFiring = true;
        isFiring = false;
    }

}

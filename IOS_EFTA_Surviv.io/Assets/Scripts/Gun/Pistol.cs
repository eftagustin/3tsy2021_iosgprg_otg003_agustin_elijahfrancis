﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pistol: Gun
{
    //Just the behavior of firing
    //No access to # of bullets/clips

    public Unit unit;

    public Bullet p_bullet;
    public GameObject p_bulletPrefab;
    public Transform firePoint;

    public Bullet new_pBullet;

    public Button pistolReloadBtn;

    void Start()
    {
        
    }
    
    public override void Fire(Ammo ammo, Unit owner)
    {
        if (ammo.currentClipCount > 0)
        {
            //Set Pistol bullet damage value
            damagePerBullet = 10;

            //Minus the # of bullet expended per fire
            ammo.currentClipCount -= 1;

            //Actual bullet instantiate
            new_pBullet = Instantiate(p_bullet, firePoint.position, firePoint.rotation);

            new_pBullet.transform.SetParent(this.transform);
            DontDestroyOnLoad(new_pBullet);

            Debug.Log("Fired pistol.");

            new_pBullet.owner = owner;
        }
        else if(ammo.currentClipCount == 0 && ammo.currentHeldCount > 0)
        {
            StartCoroutine(ReloadCoroutine(ammo));
        }
    }

    public override void Reload(Ammo ammo)
    {
        StartCoroutine(ReloadCoroutine(ammo));
    }

    IEnumerator ReloadCoroutine(Ammo ammo)
    {
        //RELOAD TIME HERE

        Debug.Log("Pistol reloading...");

        if (unit is Player)
        {
            pistolReloadBtn.interactable = false;
        }

        yield return new WaitForSeconds(1.5f);

        int reloadAmount = ammo.maxClipCount - ammo.currentClipCount;

        if (ammo.currentHeldCount < reloadAmount)
        {
            reloadAmount = ammo.currentHeldCount;
        }

        ammo.currentClipCount += reloadAmount;
        ammo.currentHeldCount -= reloadAmount;

        Debug.Log("Pistol reloaded.");

        pistolReloadBtn.interactable = true;
    }
}

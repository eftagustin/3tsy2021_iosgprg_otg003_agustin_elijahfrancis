﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    //public float fireRate;

    public float reloadSpeed;
    public int recoil;

    public int damagePerBullet;

    public virtual void Fire(Ammo ammo, Unit owner)
    {
        Debug.Log("I'm generic firing!");
    }

    public virtual void Reload(Ammo ammo)
    {
        //called by Unit
        //will be overridden by child classes
    }
}

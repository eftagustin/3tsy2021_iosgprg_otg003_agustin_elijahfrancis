﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shotgun : Gun
{
    //PLACEHOLDER

    //Just the behavior of firing
    //No access to # of bullets/clips

    public Unit unit;

    public Bullet s_bullet;
    public GameObject s_bulletPrefab;
    public Transform[] firePoints;
    public Vector3 currentEulerAngles;

    public Bullet new_sBullet;

    private int randomPositionNo;
    private float randomFactor;

    public Button reloadBtn;

    void Start()
    {

    }

    public override void Fire(Ammo ammo, Unit owner)
    {
        if (ammo.currentClipCount > 0)
        {
            //Set Pistol bullet damage value
            damagePerBullet = 20;

            //Minus the # of bullet expended per fire
            ammo.currentClipCount -= 1;

            //Actual bullet instantiate
            //make 8 

            for (int i = 0; i < 8; i++)
            {
                randomPositionNo = Random.Range(0, 12);
                randomFactor = Random.Range(0, 10);
                randomFactor *= 0.05f;

                new_sBullet = Instantiate(s_bullet, new Vector3(firePoints[randomPositionNo].position.x + randomFactor,
                                                        firePoints[randomPositionNo].position.y + randomFactor,
                                                        firePoints[randomPositionNo].position.z),
                                                        firePoints[randomPositionNo].rotation);

                new_sBullet.transform.SetParent(this.transform);
                DontDestroyOnLoad(new_sBullet);
                new_sBullet.owner = owner;

            }
        }
        else if (ammo.currentClipCount == 0 && ammo.currentHeldCount > 0)
        {
            StartCoroutine(ReloadCoroutine(ammo));
        }

        Debug.Log("Fired SHG.");
    }

    public override void Reload(Ammo ammo)
    {
        StartCoroutine(ReloadCoroutine(ammo));
    }

    IEnumerator ReloadCoroutine(Ammo ammo)
    {
        Debug.Log("SHG reloading...");

        if (unit is Player)
        {
            reloadBtn.interactable = false;
        }

        yield return new WaitForSeconds(3f);

        int reloadAmount = ammo.maxClipCount - ammo.currentClipCount;

        if (ammo.currentHeldCount < reloadAmount)
        {
            reloadAmount = ammo.currentHeldCount;
        }

        ammo.currentClipCount += reloadAmount;
        ammo.currentHeldCount -= reloadAmount;

        Debug.Log("SHG reloaded.");

        reloadBtn.interactable = true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 20f;
    public Rigidbody2D rb;
    public Unit owner;

    private void Start()
    {
        rb.velocity = transform.right * speed;
    }


    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.GetComponent<Unit>() == owner)
        {
            Debug.Log("coll with owner");
        }
        else if (collision.gameObject.GetComponent<Unit>() != owner)
        {
            if ( collision.gameObject.tag == "Enemy")
            {

                Debug.Log("Bullet hit = " + collision.gameObject.name);

                //return this later
                Destroy(this.gameObject, 2.0f);
            }
            else if(collision.tag == "Obstacle" ||
                collision.gameObject.tag == "Player")
            {
                Debug.Log("Bullet hit = " + collision.gameObject.name);

                //return this later
                Destroy(this.gameObject);
            }
        }
    }

   
}

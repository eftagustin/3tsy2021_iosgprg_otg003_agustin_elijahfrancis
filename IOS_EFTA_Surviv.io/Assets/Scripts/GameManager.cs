﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Player player;
    public static GameManager instance;

    public Transform[] moveSpots;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }

       //DontDestroyOnLoad(this);
    }
   

    public void InitPlayer(Player player)
    {
        this.player = player;
        Debug.Log("Player init in GM.");
    }

    void Update()
    {
        
    }
}

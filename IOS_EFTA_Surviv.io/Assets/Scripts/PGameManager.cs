﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System;

public class PGameManager : MonoBehaviour
{
    public static PGameManager instance;
    public Player player;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    private void Start()
    {

        StartCoroutine(AsyncLoadScene("Menu", OnMenuLoaded));
        StartCoroutine(AsyncLoadScene("Game"));
        //StartCoroutine(AsyncLoadScene("Game", OnStartGame));
    }

    void OnMenuLoaded()
    {
        MenuManager.instance.ShowCanvas(MenuType.MainMenu);
    }


    void LoadScene(string name)
    {
        StartCoroutine(AsyncLoadScene(name));
    }

    IEnumerator AsyncLoadScene(string name, Action onCallback = null)
    {
        AsyncOperation asyncLoadScene = SceneManager.LoadSceneAsync(name, LoadSceneMode.Additive);

        while (!asyncLoadScene.isDone)
        {
            //where loading bars usually is
            yield return null;
        }

        Debug.Log("Completed adding scene.");

        if(onCallback != null)
        {
            onCallback.Invoke();
        }
    }


    public void OnStartGame()
    {
        //start level
        //init level
        Debug.Log("called OSG");

        PlayerMovement.instance.SetJoystick();
        PlayerAim.instance.SetJoystick();
    }

    public void GameOver()
    {
        MenuManager.instance.ShowCanvas(MenuType.GameOver);
    }
}

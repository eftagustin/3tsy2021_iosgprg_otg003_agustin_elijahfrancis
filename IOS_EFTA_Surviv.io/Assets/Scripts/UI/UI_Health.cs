﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Health : MonoBehaviour
{
    public Player player;
    public Slider hpBar;

    private void Awake()
    {
    }

    // Start is called before the first frame update
    void Start()
    {
        //player = GameManager.instance.player;
        //player = GameObject.FindObjectOfType<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        SetPlayer();

        if (player != null)
        {
            hpBar.value = player.health.currentValue / player.health.maxValue;
        }
    }

    void SetPlayer()
    {
        if (player == null)
        {
            player = GameManager.instance.player;
        }
    }
}

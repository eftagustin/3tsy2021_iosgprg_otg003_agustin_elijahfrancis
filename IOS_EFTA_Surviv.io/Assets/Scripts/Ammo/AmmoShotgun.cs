﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoShotgun : Ammo
{

    void Start()
    {
        SetValues();
    }
    

    void Update()
    {
        
    }


    private void SetValues()
    {
        this.maxClipCount = 2;
        this.currentClipCount = 2;
    }
}

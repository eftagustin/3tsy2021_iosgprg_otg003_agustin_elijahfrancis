﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoPistol : Ammo
{

    void Start()
    {
        SetValues();
    }
    
    void Update()
    {
        
    }

    private void SetValues()
    {
        this.maxClipCount = 15;
        this.currentClipCount = 15;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour
{
    public int maxCapacity;

    //Bullets in bag
    public int currentHeldCount;

    //Bullets in current clip
    public int maxClipCount;

    //The count that depletes on shoot
    public int currentClipCount;

    void Start()
    {
        
    }
    
    void Update()
    {
        
    }
}

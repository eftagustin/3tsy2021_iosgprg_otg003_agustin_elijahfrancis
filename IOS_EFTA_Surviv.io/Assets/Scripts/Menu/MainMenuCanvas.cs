﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuCanvas : MenuCanvas
{
    public void ButtonToStart()
    {
        //async load game
    }

    public void ButtonToExit()
    {
        Application.Quit();
    }

    //public void ShowGameUI()
    //{
    //    MenuManager.instance.ShowCanvas(MenuType.Game);
    //}

    public void Play()
    {
        MenuManager.instance.ShowCanvas(MenuType.Game);
        PGameManager.instance.OnStartGame();
        EnemySpawner.instance.SpawnEach();
    }
}



﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioSource battleBGM;
    public AudioSource bossBGM;
    public AudioSource lifeUpSound;
    public AudioSource dashSound;
    public AudioSource itemSpawnSound;
    public AudioSource playerGetHitSound;
    public AudioSource playerAttackSound;
    public AudioSource bossSpawnSound;
    public AudioSource gameOverSound;

    public static AudioManager instance;

    private void Awake()
    {

        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        PlayBattleTheme();
    }

    public void InitValues(AudioSource battleMusic, AudioSource bossMusic,
                           AudioSource lifeTrack, AudioSource dashTrack, 
                           AudioSource itemTrack, AudioSource playerHitTrack,
                           AudioSource playerAttackTrack, AudioSource bossSpawnTrack,
                           AudioSource gameOverTrack)
    {
        this.battleBGM = battleMusic;
        this.bossBGM = bossMusic;
        this.lifeUpSound = lifeTrack;
        this.dashSound = dashTrack;
        this.itemSpawnSound = itemTrack;
        this.playerGetHitSound = playerHitTrack;
        this.playerAttackSound = playerAttackTrack;
        this.bossSpawnSound = bossSpawnTrack;
        this.gameOverSound = gameOverTrack;
    }

    public void PlayBossTheme() {
        Debug.Log("Playing boss theme");
        battleBGM.Pause();
        bossBGM.Play();
    }

    public void PlayBattleTheme()
    {
        Debug.Log("Playing battle theme");
        bossBGM.Pause();
        battleBGM.Play();
    }
        
}

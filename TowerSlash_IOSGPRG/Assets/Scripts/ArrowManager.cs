﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArrowManager : MonoBehaviour
{
    private int arrowDirection;
    private int playerDirection;

    public Character character;
    public SpawnerManager spawnerManager;
    public static ArrowManager instance;

    void Awake()
    {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }

            DontDestroyOnLoad(gameObject);

        //Boss.instance.InitArrowManager(this);
    }

    public void InitPlayer(Character character)
    {
        this.character = character;

    }

    public void InitSpawnManager(SpawnerManager spawnerManager)
    {
        this.spawnerManager = spawnerManager;
    }

    public int GenerateArrow()
    {
      
        arrowDirection = Random.Range(1, 9);

        Debug.Log("Set and sent direction of arrow.");
        return arrowDirection;
    }

    public void GetPlayerSwipe(int playerInputDirection)
    {
        if (playerInputDirection != 0)
        {
            Debug.Log("Got player swipe of " + playerInputDirection);
        }
        
            playerDirection = playerInputDirection;
    }


    public void CompareDirection(int enemyDirection, int playerDirection, GameObject enemy)
    {

        Debug.Log("CompareDir reached; enemyDir is: " + enemyDirection);

        Debug.Log("Char has swiped" + character.hasSwiped);
        Debug.Log("Enemy dir " + enemyDirection + "; Player dir =" + playerDirection);

        if (character.isInRange)
        {

            if (character.hasSwiped && enemyDirection == playerDirection)
            {
                spawnerManager.RemoveEnemy(enemy.gameObject);
                Destroy(enemy.gameObject);
                
                Debug.Log("Destroyed enemy in range.");

                character.hasSwiped = false;

                character.playerInputDirection = 0;

                AudioManager.instance.playerAttackSound.Play();
            }
            else if (enemyDirection != playerDirection)
            {
                Debug.Log("Player has taken damage.");
                character.PlayerTakeDamage();
            }
        }

        character.isInRange = false;
    }

    public bool CompareDirectionBoss(int enemyDirection, int playerDirection, GameObject enemy)
    {

        Debug.Log("CompareDirBoss reached; enemyDir is: " + enemyDirection);

        Debug.Log("Char has swiped" + character.hasSwiped);
        Debug.Log("Enemy dir " + enemyDirection + "; Player dir =" + playerDirection);

        if (character.isInRange)
        {

            if (character.hasSwiped && enemyDirection == playerDirection)
            {
                
                Debug.Log("Damaged boss");

                character.hasSwiped = false;
                character.playerInputDirection = 0;

                AudioManager.instance.playerAttackSound.Play();

                return true;
            }
            else if (enemyDirection != playerDirection)
            {
                Debug.Log("Player has taken damage.");
                character.PlayerTakeDamage();

                character.hasSwiped = false;
                character.playerInputDirection = 0;

                return false;
            }
        }

        character.isInRange = false;

        return false;
    }
}

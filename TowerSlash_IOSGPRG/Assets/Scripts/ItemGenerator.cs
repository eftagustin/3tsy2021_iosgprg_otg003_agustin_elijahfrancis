﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemGenerator : MonoBehaviour
{
    public GameObject dashItem;
    private Character character;
    private bool canSpawnItem;
    private int randomChance;
    public static ItemGenerator instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        canSpawnItem = false;
        
    }

    void Update()
    {
        CheckScore();
        CheckCharacterStatus();
    }

    public void InitPlayer(Character character)
    {
        this.character = character;

    }

    private void CheckScore()
    {
        randomChance = Random.Range(0, 100);

        if (GameManager.instance.score % 250 == 0 && randomChance < 20)
        {
            canSpawnItem = true;

            Debug.Log("Rand chance is" + randomChance);

        };
    }

    private void CheckCharacterStatus()
    {
        if(character.dashCharges < character.superDashMax && canSpawnItem)
        {
            GameObject newItem = (GameObject)Instantiate(dashItem,
                                 new Vector3(character.transform.position.x,
                                 character.transform.position.y + Random.Range(10, 30),
                                 character.transform.position.z), Quaternion.identity);

            canSpawnItem = false;

            AudioManager.instance.itemSpawnSound.Play();
        }
    }
}

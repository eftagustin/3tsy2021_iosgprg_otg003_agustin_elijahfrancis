﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{
    [SerializeField] private AudioSource battleMusic;
    [SerializeField] private AudioSource bossMusic;
    [SerializeField] private AudioSource itemSpawnSound;
    [SerializeField] private AudioSource lifeUpSound;
    [SerializeField] private AudioSource dashSound;
    [SerializeField] private AudioSource playerGetHitSound;
    [SerializeField] private AudioSource playerAttackSound;
    [SerializeField] private AudioSource bossSpawnSound;
    [SerializeField] private AudioSource gameOverSound;

    private void Awake()
    {
        AudioManager.instance.InitValues(battleMusic, bossMusic, 
                                         lifeUpSound, dashSound, 
                                         itemSpawnSound, playerGetHitSound,
                                         playerAttackSound, bossSpawnSound, gameOverSound);
    }
}

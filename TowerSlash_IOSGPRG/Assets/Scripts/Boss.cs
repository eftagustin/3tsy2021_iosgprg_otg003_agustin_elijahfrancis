﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : Enemy
{
    private bool isBossHit;
    private int bossUpMoveOnHit;
    [SerializeField] private int bossDirection;
    [SerializeField] private int bossDirectionImage;

    public static Boss instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        SetValues();
        SetEnemyStats();
    }

    void Update()
    {
        CheckPlayerStatus(character);
    }

    private void SetValues()
    {
        this.enemyHealth = 3;
        isBossHit = false;

        character = GameManager.instance.character;
        arrowManager = GameManager.instance.arrowManager;
    }

    public void InitPlayer(Character character)
    {
        this.character = character;
    }

    public void InitArrowManager(ArrowManager arrowManager)
    {
        this.arrowManager = arrowManager;
    }

    public override void CheckPlayerStatus(Character playerColliding)
    {
        //If PLAYER does not kill enemy, and it reaches its Y position:
        if (character.transform.position.y >= enemy.transform.position.y)
        {
            //bad sound effect here

            character.PlayerTakeDamage();
        }

        if (character.isInRange && enemy.enemyIsInRange)
        {

            if (enemy.isBigger == false)
            {
                Vector3 newSizeAddition = new Vector3(0.25f, 0.25f, 0.25f);
                enemy.spriteRenderer.transform.localScale = newSizeAddition;

                enemy.isBigger = true;
            }
            if (character.hasSwiped)
            {
                isBossHit = arrowManager.CompareDirectionBoss(bossDirection, character.playerInputDirection, this.gameObject);
                Debug.Log("BossHit is " + isBossHit);
                BossHitCheck();
            }
        }
    }

    //if boss is hit, need to set a new image

    private void BossHitCheck()
    {
        if (isBossHit)
        {
            this.EnemyTakeDamage();
            GameManager.instance.AddBossHitMult();

            this.SetEnemyStats();
            MoveBoss();

            isBossHit = false;
        }
    }

    private void MoveBoss()
    {
        bossUpMoveOnHit = Random.Range(6, 12);
        this.transform.position += new Vector3(0, bossUpMoveOnHit ,0);
    }

    public override void SetEnemyStats()
    {
        this.isRotating = this.SetRotating();

        //NORMAL, non-rotating arrow
        if (!this.isRotating)
        {
            Debug.Log(this.gameObject.name + "Am ENEMY not rotating, am set!");

            bossDirection = arrowManager.GenerateArrow();
            bossDirectionImage = bossDirection;

            if (bossDirection >= 5)
            {
                bossDirection -= 4;
            }

            Debug.Log(this.gameObject.name + "NOTROT Boss direction is " + bossDirection);
        }
        else if (this.isRotating)
        {
            bossDirection = arrowManager.GenerateArrow();

            if (bossDirection >= 5)
            {
                bossDirection -= 4;
            }
        }

        SetImage(bossDirectionImage);
    }

    public override void OnTriggerEnter(Collider collision)
    {
        character.isInRange = true;

        if (collision.gameObject.tag == "Player")
        {
            this.enemyIsInRange = true;
            SetImage(bossDirectionImage);

            this.isRotating = false;
        }

    }

    public override void EnemyTakeDamage()
    {
        this.enemyHealth -= 1;

        if( this.enemyHealth <= 0)
        {
            character.isInBossSequence = false;
            character.MoveCharacterToStart();

            //spawnerManager.RemoveEnemy(enemy.gameObject);
            Destroy(this.gameObject);
            Debug.Log("Boss has been slain!!");

            AudioManager.instance.PlayBattleTheme();

        }
    }

    public override void SetImage(int index)
    {
        if (images.Length >= index)
        {
            this.spriteRenderer.sprite = images[index];

        }
        else
        {
            Debug.LogError("Invalid image index: " + index);
        }

    }
    //if boss dies remove from list...
}

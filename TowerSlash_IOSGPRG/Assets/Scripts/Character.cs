﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Character : MonoBehaviour
{
    //VARIABLES

    [SerializeField] private Transform startingPos;

    //public CharacterSwipe charSwipe_;

    //a. CONTROLLER
    private CharacterController controller;
    private Vector3 moveDirection;

    //b. STATS
    public float playerSpeed = 0;

    //c. SWIPE
    //CharacterSwipe characterSwipe;

    protected Vector2 fingerDownPos;
    protected Vector2 fingerUpPos;
    [SerializeField] protected bool swipeReleaseDetect = false;
    [SerializeField] protected float minDistanceForSwipe = 20f;
    public int playerInputDirection = 0;

    public bool hasSwiped = false;
    public bool isTapping;

    public LineRenderer lineRenderer;
    protected float zOffset = 10;

    public static event Action<SwipeData> OnSwipe = delegate { };

    //d. ArrowManager

    //ArrowManager arrowManager;
    public bool isInRange;

    //e. Spawning
    /// <summary>
    /// if score reaches a point, spawn manager will no longer spawn, and will instead spawn the boss
    /// </summary>
    public bool isInBossSequence;
    public bool isMovingToStartPos;
    public bool isInDontSpawnArea;

    //f. Features
    public int playerLives = 0;
    public int dashCharges = 0;
    public int playerLivesMax = 0;
    public int playerLivesMin = 0;
    public int superDashMax = 0;
    public int superDashMin = 0;
    public ParticleSystem dashParticles;

    public bool isSuperDashing;
    protected float dashMultiplier;
    protected float superDashMultiplier;
    

    public struct SwipeData
    {
        public Vector2 StartPosition;
        public Vector2 EndPosition;
        public SwipeDirection Direction;
    }

    public enum SwipeDirection
    {
        None,
        Up,
        Down,
        Left,
        Right
    }

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
        OnSwipe += SwipeDetector_OnSwipe;
    }

    void Start()
    {
        controller = GetComponent<CharacterController>();
        //charSwipe_ = GetComponent<CharacterSwipe>();
        SetPlayerStats();

        ///this worked
        ///so what happened is, everytime character starts,
        ///game manager takes Char char as a parameter
        GameManager.instance.InitPlayer(this);
        ArrowManager.instance.InitPlayer(this);
        SpawnerManager.instance.InitPlayer(this);
        ItemGenerator.instance.InitPlayer(this);
        //Boss.instance.InitPlayer(this);
        

        isInRange = false;
        isTapping = false;
        isInBossSequence = false;
        isSuperDashing = false;
    }


    //Swipe call goes here
    //chain of events
    //PlayerSwipe -> DetectSwipe -> SendSwipe
    void Update()
    {
        PlayerSwipe();

        ArrowManager.instance.GetPlayerSwipe(playerInputDirection);

    }
    
    private void FixedUpdate()
    {
        PlayerUpwardMovement();
    }

    private void PlayerUpwardMovement()
    {
        //constant Upward movement
        moveDirection.y = playerSpeed;
        controller.Move(moveDirection * Time.deltaTime);
        
    }

    private void SetPlayerStats()
    {
        //Initialize stat values
        playerLives = 1;
        playerSpeed = 3f;
        dashCharges = 1;

        playerLivesMax = 3;
        playerLivesMin = 1;
        superDashMax = 3;
        superDashMin = 0;
        dashMultiplier = 1.0f;
        superDashMultiplier = 3.0f;
}


    
     public void PlayerSwipe() {
        hasSwiped = false;


        foreach (Touch touch in Input.touches)
        {
            if(touch.phase == TouchPhase.Began)
            {
                fingerUpPos = touch.position;
                fingerDownPos = touch.position;
            }

            if(!swipeReleaseDetect && touch.phase == TouchPhase.Moved)
            {
                fingerDownPos = touch.position;
                DetectSwipe();
            }

            if(touch.phase == TouchPhase.Ended)
            {
                fingerDownPos = touch.position;
                DetectSwipe();
            }
            
            if (fingerDownPos == fingerUpPos && !isTapping)
            {
                
                isTapping = true;
                DetectSwipe();
            }
        }
    }
    

    private void DetectSwipe()
    {
        if (SwipeDistanceMet())
        {
            if (IsVertical())
            {
                var direction = fingerDownPos.y - fingerUpPos.y > 0 ? SwipeDirection.Up : SwipeDirection.Down;
                SendSwipe(direction);
            }
           
            else
            {
                var direction = fingerDownPos.x - fingerUpPos.x > 0 ? SwipeDirection.Right : SwipeDirection.Left;
                SendSwipe(direction);
            }

            fingerUpPos = fingerDownPos;
            
        }
        else if(isTapping)
            {
                dashAction(dashMultiplier);       
            }   
    }
    

    protected void dashAction(float multiplier)
    {
        Vector3 dashPosition = new Vector3(0, 2f*multiplier, 0);

        Debug.Log("old y = " + controller.transform.position.y);

        GameManager.instance.TapDashAddScore();
        controller.Move(dashPosition);

        Debug.Log("new y = " + controller.transform.position.y);

        StartCoroutine(Wait());
    }

    IEnumerator Wait()
    {
        Debug.Log("Waiting..." + isTapping);
        yield return new WaitForSeconds(3.5f);
        Debug.Log("Have waited..." + isTapping);

        isTapping = false;
    }

    public void DashButtonTap()
    {

        if (!isInRange && dashCharges > 0)
        {
            dashParticles.Play();

            //TO DO add sound
            dashAction(superDashMultiplier);
            AudioManager.instance.dashSound.Play();
            GameManager.instance.SuperDashAddScore();
            dashCharges -= 1;
            Debug.Log(dashCharges + " = dashCharges");
        }
    }

    
    private bool IsVertical()
    {
        return VerticalMovementDistance() > HorizontalMovementDistance();
    }

    private bool SwipeDistanceMet()
    {
        return VerticalMovementDistance() > minDistanceForSwipe || HorizontalMovementDistance() > minDistanceForSwipe;
    }

    private float VerticalMovementDistance()
    {
        return Mathf.Abs(fingerDownPos.y - fingerUpPos.y);
    }

    private float HorizontalMovementDistance()
    {
        return Mathf.Abs(fingerDownPos.x - fingerUpPos.x);
    }

    
    protected void SwipeDetector_OnSwipe(SwipeData data)
    {

        Vector3[] positions = new Vector3[2];
        positions[0] = Camera.main.ScreenToWorldPoint(new Vector3(data.StartPosition.x, data.StartPosition.y, zOffset));
        positions[1] = Camera.main.ScreenToWorldPoint(new Vector3(data.EndPosition.x, data.EndPosition.y, zOffset));
        lineRenderer.positionCount = 2;
        lineRenderer.SetPositions(positions);

        Debug.Log("Swipe in Direction" + data.Direction);
    }
    

    private void SendSwipe(SwipeDirection direction)
    {
        SwipeData swipeData = new SwipeData()
        {
            Direction = direction,
            StartPosition = fingerDownPos,
            EndPosition = fingerUpPos
        };

        //OnSwipe(swipeData);

        playerInputDirection = (int)swipeData.Direction;

        if (playerInputDirection != 0)
        {
            hasSwiped = true;
        }
     
    }

    

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            Debug.Log("Player encountered " + other.gameObject.name);

            //for testing
            //SpawnerManager.Instance.RemoveEnemy(other.gameObject);
            //Destroy(other.gameObject);
        }
        else if(other.gameObject.tag == "Waypoint" && !isInBossSequence)
        {
            Debug.Log("Met Atomos...");

            MoveCharacterToStart();
        }
        else if(other.gameObject.tag == "DontSpawn" && !isInBossSequence)
        {
            Debug.Log("In dont spawn");
            this.isInDontSpawnArea = true;
            this.isInRange = false;
        }
        else if(other.gameObject.tag == "Item")
        {
            this.dashCharges += 1;
            Destroy(other.gameObject);
        }
    }

    public void MoveCharacterToStart()
    {
        isInRange = false;
        isMovingToStartPos = true;

        this.controller.enabled = false;
        this.transform.position = startingPos.transform.position;
        controller.transform.position = startingPos.transform.position;
        this.controller.enabled = true;

    }

    //called when Swipe fails
    public void PlayerTakeDamage()
    {
        AudioManager.instance.playerGetHitSound.Play();
        playerLives -= 1;
        Debug.Log("Player took damage. Current health is " + playerLives);

        if (playerLives <= 0)
        {
            //Destroy(this.gameObject);
            this.gameObject.SetActive(false);

            OnSwipe -= SwipeDetector_OnSwipe;

            //charSwipe_.UnsubscribeEvent();

            //show Game Over screen
            //direct to score screen
        }
    }
}

///source
/// Jason Weimann - Youtube, Easy Swipe Detection in Unity3D
/// For Swipe detection and display
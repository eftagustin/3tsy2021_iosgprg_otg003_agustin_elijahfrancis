﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public Character character;
    public ArrowManager arrowManager;

    public int scoreCounter;
    public int score;
    private int dashBonus;
    private int tapBonus;
    private int dashBonusMultiplier;
    private int tapDashMultiplier;
    private int bossHitBonus;
    private int bossHitMultiplier;

    public bool isGameActive;
    public bool isGameStart;

    private bool bossTrigger;
    private bool hasCalledGameOver;
    private int scoreInterval;

    //public AudioSource lifeUpSound;

    public static GameManager instance;

    private void Awake()
    {

        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);


        instance.character = character;
    }


    void Start()
    {
        SetValues();
    }
    
    void Update()
    {
        UpdateScore();

        CheckBossStatus();
        ScorePlayerHeartUp();
        GameOverCheck();
    }

    public void InitPlayer(Character character)
    {
        this.character = character;

        SetValues();
        Debug.Log("GameManager: Game started");
        
    }

    private void UpdateScore()
    {
        if (isGameActive)
        {
            score = (scoreCounter) + (dashBonus*dashBonusMultiplier) 
                + (tapBonus*tapDashMultiplier) 
                + (bossHitBonus*bossHitMultiplier);
            scoreCounter++;
        }
    }

    private void GameOverCheck()
    {
        if(character.playerLives <= 0 && !hasCalledGameOver)
        {
            isGameActive = false;
            AudioManager.instance.battleBGM.Stop();
            AudioManager.instance.gameOverSound.Play();
            hasCalledGameOver = true;
        }
    }

    //FEATURE ONE
    private void ScorePlayerHeartUp()
    {
        if (isGameStart)
        {
            isGameStart = false;
        }

        else if(!isGameStart && score%2000 == 0 && character.playerLives != character.playerLivesMax)
        {
            character.playerLives += 1;
            AudioManager.instance.lifeUpSound.Play();
        }
    }

    private void SetValues()
    {

        instance.character = character;
        Debug.Log("reset");
        scoreCounter = 0;
        isGameActive = true;
        isGameStart = true;
        hasCalledGameOver = false;
        bossTrigger = false;
        scoreInterval = 1;
        bossHitBonus = 350;
        bossHitMultiplier = 0;
        score = 0;
        dashBonus = 750;
        tapBonus = 150;
        dashBonusMultiplier = 0;
        tapDashMultiplier = 0;
    }



    private void CheckBossStatus()
    {
        if(score >= 6000*scoreInterval)
        {
            bossTrigger = true;
            scoreInterval++;

            character.playerSpeed += 0.25f;
        }

        if(bossTrigger)
        {
            SetBossState();
        }
    }

    //use events here^

    public void AddBossHitMult()
    {
        bossHitMultiplier += 1;
        Debug.Log("Added to boss hit mult");
        Debug.Log(bossHitMultiplier);
    }

    private void SetBossState()
    {
        character.isInBossSequence = true;
        bossTrigger = false;
        //remember to turn it off when boss dies
    }

    public void TapDashAddScore()
    {
        tapDashMultiplier += 1;
    }

    public void SuperDashAddScore()
    {
        dashBonusMultiplier += 1;
    }

    //FEATURE TWO
    //increase score when dashing
    //and show that
}

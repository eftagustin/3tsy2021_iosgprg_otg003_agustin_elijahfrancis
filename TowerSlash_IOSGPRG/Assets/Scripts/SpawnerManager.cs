﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerManager : MonoBehaviour
{
    [SerializeField] private List<Enemy> enemyList = new List<Enemy>();

    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private GameObject bossPrefab;

    public Character character;
    public static SpawnerManager instance;

    /// <summary>
    /// Don't really need randX, since all of our enemies spawn in a straight line.
    /// For positioning enemies, iterate every few Y.
    /// 
    /// </summary>

    private float posX;
    private int intervalCount;

    private bool bossPresent;
    private int bossHealthAddition;
    private int bossSpawnCount;

    [SerializeField] private int enemyCount;

    void Awake()
    {
        if (instance == null)
       {
           instance = this;
       }
       else
        {
            Destroy(gameObject);
       }

       DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        ArrowManager.instance.InitSpawnManager(this);

        SetValues();

        //do at start of scene
        //AssignCharacter();
        //Debug.Log("SM assigned enemies");
        //Debug.Log("GameStart is" + GameManager.instance.isGameStart);
        //AssignEnemy();
    }

    private void Update()
    {
        //AssignCharacter();
        //AssignEnemy();
        CheckEnemyList();
        CheckCharacterStatus();
    }

    private void SetValues()
    {
        enemyCount = 6;
        posX = -4.0f;
        bossPresent = false;
        bossHealthAddition = 2;
        bossSpawnCount = 0;
    }

    public void InitPlayer(Character character)
    {
        this.character = character;

    }

    private void SpawnEnemies(int enemyCount)
    {
        for (int i = 0; i < enemyCount; i++)
        {
            intervalCount = i + 1;

            //Add numbers to current char position, so no matter where char is, enemies can be spawned further up
            float posY = Random.Range(3, 8) + (intervalCount * 15) + GameManager.instance.character.transform.position.y;

            GameObject newEnemy = Instantiate(enemyPrefab, new Vector3(posX, posY, 0), Quaternion.identity);
            Enemy enemy = newEnemy.GetComponent<Enemy>();
            enemy.Health = 1;

            AddEnemyToList(enemy);
        }
    }

    private void SpawnBoss()
    {
        float posY = Random.Range(10, 20) + GameManager.instance.character.transform.position.y;

        GameObject newEnemy = Instantiate(bossPrefab, new Vector3(posX, posY, 0), Quaternion.identity);
        Boss boss = newEnemy.GetComponent<Boss>();

        ///add to boss health,
        ///so each time it spawns
        ///its HP increases
        boss.enemyHealth += bossHealthAddition * bossSpawnCount;

        EmptyEnemyList();
        AddEnemyToList(boss);

        bossPresent = true;
        bossSpawnCount++;

        AudioManager.instance.PlayBossTheme();
        AudioManager.instance.bossSpawnSound.Play();
    }

    private void CheckCharacterStatus()
    {
        if (character.isMovingToStartPos)
        {
            //can play sound

            Debug.Log("Cleared enemies coz player moved.");
            EmptyEnemyList();

            character.isMovingToStartPos = false;
            character.isInDontSpawnArea = false;
        }
        else if (character.isInDontSpawnArea && !character.isInBossSequence)
        {
            //can plauy a sound here

            Debug.Log("Cleared enemies coz player is in DontSpawn.");
            EmptyEnemyList();
        }
    }

    private void EmptyEnemyList()
    {
        foreach (Enemy enemyInList in enemyList)
        {
            if (enemyInList != null)
            {
                Destroy(enemyInList.gameObject);
            }
        }

        enemyList.Clear();
    }

    private void CheckEnemyList()
    {
        if (!character.isInBossSequence)
        {
            //when boss dies, BossTakeDamage will set char.isInBS to false
            //so we'll set this here, since we're encountering normal mobs again
            bossPresent = false;

            if (enemyList.Count > enemyCount)
            {
                EmptyEnemyList();
            }
            else if (enemyList.Count == 0
            && !character.isInDontSpawnArea)
            {
                Debug.Log("Enemy list is empty. Will populate.");
                SpawnEnemies(enemyCount);
            }
        }
        else if(!bossPresent)
        {
            //bossPresent is to prevent multiple spawning of the boss
            Debug.Log("Boss Spawn called at " + GameManager.instance.score);
            SpawnBoss();
        }
        
        
        if(character.playerLives == 0)
        {
            //this will clear list if player dies
            EmptyEnemyList();
        }
        
    }

    private void AddEnemyToList(Enemy enemy)
    {
        enemyList.Add(enemy);
    }

    
    public void RemoveEnemy(GameObject gameObject)
    {
        enemyList.Remove(gameObject.GetComponent<Enemy>());
    }
   
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public float enemyHealth;

    public SpriteRenderer spriteRenderer;
    public Sprite[] images;
    public ArrowManager arrowManager;
    public int enemyDirection;
    public int enemyImageDirection;
    private GameObject playerColliding;
    public Character character;
    public Enemy enemy;

    /// HERE or on AM?
    public bool isRotating;
    public int isRotatingInt;
    public bool isReversed;

    public int counter;
    public bool enemyIsInRange;
    public bool isBigger;

    public SpawnerManager spawnerManager;

    private void Awake()
    {
        //character = (Character)FindObjectOfType(typeof(Character));
        //arrowManager = (ArrowManager)FindObjectOfType(typeof(ArrowManager));
        //counter = 0;

        character = GameManager.instance.character;
        arrowManager = GameManager.instance.arrowManager;

        isBigger = false;
    }

    
    void Start()
    {
        SetEnemyStats();
        StartCoroutine(Timer());
    }
    
    void Update()
    {
        CheckEnemyStatus();
        CheckPlayerStatus(character);
    }

    IEnumerator Timer()
    {
        while (!enemy.enemyIsInRange && enemy.isRotating)
        {
            SetImage(Random.Range(1, 9));
            yield return new WaitForSecondsRealtime(1.5f);
        }
    }

    private void CheckEnemyStatus()
    {
        if (enemy.isRotating && enemy.enemyIsInRange)
        {
            Vector3 newSizeAddition = new Vector3(1.2f, 1.2f, 1.0f);

            enemy.spriteRenderer.transform.localScale = newSizeAddition;

            enemy.isRotating = false;
        }
    }

    virtual public void SetEnemyStats()
    {
        enemy.enemyHealth = 1;
        enemy.isRotating = enemy.SetRotating();

        //NORMAL, non-rotating arrow
        if (!enemy.isRotating)
        {
            Debug.Log(enemy.gameObject.name + "Am ENEMY not rotating, am set!");

                enemy.enemyDirection = arrowManager.GenerateArrow();
                enemy.enemyImageDirection = enemy.enemyDirection;

                if (enemy.enemyDirection >= 5)
                {
                     enemy.enemyDirection -= 4;
                }

                     enemy.SetImage(enemy.enemyImageDirection);

                Debug.Log(this.gameObject.name + "NOTROT Enemy direction is " + enemy.enemyDirection);
        }
        else if (enemy.isRotating)
            {
                enemy.enemyDirection = arrowManager.GenerateArrow();
                enemy.enemyImageDirection = enemy.enemyDirection;

                if (enemy.enemyDirection >= 5)
                {
                    enemy.enemyDirection -= 4;
                }
            }
    }


    virtual public void EnemyTakeDamage()
    {
        enemy.enemyHealth -= 1;

        if (enemy.enemyHealth <= 0)
        {
            //spawnerManager.RemoveEnemy(enemy.gameObject);
            Destroy(enemy.gameObject);
            Debug.Log("An enemy has been slain!!");
        }
    }

    //this, for rotating arrows stabilizing when player comes into contact
    //move this 

   virtual public void OnTriggerEnter(Collider collision)
    {
        character.isInRange = true;

        if (collision.gameObject.tag == "Player")
            {
            enemy.enemyIsInRange = true;
            //enemyImageDirection = enemyDirection;
            SetImage(enemy.enemyImageDirection);
        
            enemy.isRotating = false;
        }
        
    }

    //this function should be overriden
    //why does it still function
    virtual public void CheckPlayerStatus(Character playerColliding)
    {
        //If PLAYER does not kill enemy, and it reaches its Y position:
        if (character.transform.position.y >= enemy.transform.position.y)
        {
            //bad sound effect here

            character.PlayerTakeDamage();

            //enemy takes damage too, coz by design a player has lost a heart already
            //the enemy must be cleared even if the player made a mistake
            
            EnemyTakeDamage();
        }

        if (character.isInRange && enemy.enemyIsInRange)
        {

            if (enemy.isBigger == false)
            {
                Vector3 newSizeAddition = new Vector3(0.5f, 0.5f, 0.5f);
                enemy.spriteRenderer.transform.localScale = newSizeAddition;

                enemy.isBigger = true;
            }
            if (character.hasSwiped)
            {
                enemy.enemyIsInRange = false;
                Debug.Log("player input is" + character.playerInputDirection);
                arrowManager.CompareDirection(enemy.enemyDirection, character.playerInputDirection, this.gameObject);
            }
        }
    }
    
    public bool SetRotating()
    {
        enemy.isRotatingInt = Random.Range(0, 2);

        Debug.Log("int is " + isRotatingInt);

        if(enemy.isRotatingInt == 0)
        {
            enemy.isRotating = false;
            Debug.Log(enemy.gameObject.name + "isRotating " + enemy.isRotating);
            return enemy.isRotating;
        }
        else
        {
            
            isRotating = true;
            Debug.Log(enemy.gameObject.name + "isRotating " + enemy.isRotating);
            return enemy.isRotating;
        }
    }


    virtual public void SetImage(int index)
    {
        if (images.Length >= index)
        {
            enemy.spriteRenderer.sprite = images[index];

        }
        else
        {
            Debug.LogError("Invalid image index: " + index);
        }
        
    }

    public float Health {  get { return enemyHealth; } set { enemyHealth = value; } }
}

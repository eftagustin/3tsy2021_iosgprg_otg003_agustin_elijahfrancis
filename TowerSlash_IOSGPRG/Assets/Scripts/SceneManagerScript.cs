﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class SceneManagerScript : MonoBehaviour
{
    public Button startButton;
    public Button quitButton;

    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        GameManager.instance.isGameStart = true;
        GameManager.instance.isGameActive = true;
    }

    public void RetryGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        GameManager.instance.isGameStart = true;
    }

    public void QuitGame()
    {
        SceneManager.LoadScene(0);
        GameManager.instance.isGameStart = false;
    }
}

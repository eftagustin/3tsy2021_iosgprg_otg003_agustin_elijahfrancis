﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameUI : MonoBehaviour
{
    //[SerializeField] private Character character;

    public Character character;

    public Image[] heartIcons;

    public Image[] superDashIcons;

    public Button dashButton;

    public TextMeshProUGUI scoreText;

    public GameObject gameOverScreen;
    public TextMeshProUGUI scoreScreenText;


    void Start()
    {
        gameOverScreen.SetActive(false);
    }


    void LateUpdate()
    {
        UpdateHearts();
        UpdateDashes();
        CheckDashButtonStatus();
        UpdateScoreText();
        GameOverScreenCheck();
    }

    private void UpdateHearts()
    {
        for(int i = 0; i < character.playerLivesMax; i++)
        {
            int j = i + 1 ;

            if(j <= character.playerLives)
            {
                heartIcons[i].enabled = true;
            }
            if(j > character.playerLives)
            {
                heartIcons[i].enabled = false;
            }
        }
    }

    private void CheckDashButtonStatus()
    {
        if (character.isInRange)
        {
            dashButton.interactable = false;
        }
        else
        {
            dashButton.interactable = true;
        }
    }

    private void UpdateDashes()
    {
        for (int i = 0; i < character.superDashMax; i++)
        {
            int j = i + 1;

            if (j <= character.dashCharges)
            {
                superDashIcons[i].enabled = true;
            }
            if (j > character.dashCharges)
            {
                superDashIcons[i].enabled = false;
            }
        }

        if(character.dashCharges <= 0)
        {
            dashButton.gameObject.SetActive(false);
        }
        else
        {
            dashButton.gameObject.SetActive(true);
        }
    }


    private void UpdateScoreText()
    {
        scoreText.text = GameManager.instance.score.ToString();
    }

    private void GameOverScreenCheck()
    {
        if(!GameManager.instance.isGameActive)
        {
            scoreScreenText.text = GameManager.instance.score.ToString();
            gameOverScreen.SetActive(true);

        }
        else if (GameManager.instance.isGameActive)
        {
            gameOverScreen.SetActive(false);
        }
    }

 
}
